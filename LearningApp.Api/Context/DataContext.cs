using LearningApp.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace LearningApp.Api.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }

        public DbSet<User> Users { get; set; }
    }
}