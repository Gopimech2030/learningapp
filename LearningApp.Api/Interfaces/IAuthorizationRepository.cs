using System.Threading.Tasks;
using LearningApp.Api.Models;

namespace LearningApp.Api.Interfaces
{
    public interface IAuthorizationRepository
    {
        Task<User> Register(User user, string password);

        Task<User> Login(string userName, string password);

        Task<bool> UserExists(string userName);
    }
}