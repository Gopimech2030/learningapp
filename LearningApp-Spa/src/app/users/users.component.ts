import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getValues();
  }

  getValues() {
    return this.httpClient.get('http://localhost:5000/api/users').subscribe( respose => { this.users = respose; }, error => {console.log( error )});
  }

}
