import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { NavComponent } from './nav/nav.component';
import { AuthorizationService } from './services/authorization.service';

@NgModule({
  declarations: [AppComponent, UsersComponent, NavComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [AuthorizationService],
  bootstrap: [AppComponent],
})
export class AppModule { }
