import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  model: any = {};

  constructor(private authorizationService: AuthorizationService) {}

  ngOnInit(): void {}

  login() {
    this.authorizationService.login(this.model).subscribe(
      (next) => {
        console.log('Logged in successfully');
      },
      (error) => {
        console.log('Login failed');
      }
    );
  }

  loggedIn(){
    const token = localStorage.getItem("token");
    return !!token;
  }

  logout(){
    localStorage.removeItem("token");
    console.log("Logged out");
  }
}
